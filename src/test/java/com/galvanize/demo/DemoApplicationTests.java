package com.galvanize.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.GetMapping;

import static com.galvanize.demo.DemoApplication.returnNumber;

@SpringBootTest
class DemoApplicationTests {

	@Test

	public void returnNumberShouldReturnOne()
	{
		returnNumber();
	}

	@Test
	void contextLoads() {
	}

}
